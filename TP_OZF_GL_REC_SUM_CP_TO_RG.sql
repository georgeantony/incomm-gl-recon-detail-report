/*--- $Header:TP_OZF_GL_REC_RPT_CP_TO_RG.sql 1.0 12-sep-13 11:37:30 AM PKS $
--- Name:  TP_OZF_GL_REC_RPT_CP_TO_RG.sql
--- Program Type:  Concurrent Program to request Group
--- Notes:
--- History:
--- ===========================================================================================
--- |     DATE         |    Owner                                   |   Activity
--- ===========================================================================================
--- |  03-Nov-13       |    Tribus Point                            |   Created initial script.
--- ===========================================================================================*/
SET serveroutput ON size 1000000;
DECLARE
  RG      VARCHAR2(200) :='';
  RG_APPS VARCHAR2(20)  :='';
  CP      VARCHAR2(200) :='';
  CP_APPS VARCHAR2(20)  :='';
  RSET     VARCHAR2(2000) := null;
  RSET_APPS VARCHAR2(2000) := null;
  l_line    number ;
BEGIN
  l_line  :=1;
  CP      := 'TP_GL_RECON_DTL_RPT';
  CP_APPS := 'TRIBUS';
  RG      := 'Trade Management Request Group';
  RG_APPS := 'OZF';
  IF ( FND_PROGRAM.REQUEST_GROUP_EXISTS ( rg, RG_APPS)) THEN
    DBMS_OUTPUT.PUT_LINE(rg||' Request Group Exists');
    IF (fnd_program.program_in_group(CP, CP_APPS, RG, RG_APPS)) THEN
      DBMS_OUTPUT.PUT_LINE(CP||' Program already exists in '||rg);
      NULL;
    ELSE
      fnd_program.add_to_group ( program_short_name => CP, program_application => CP_APPS, request_group => RG, group_application => RG_APPS);
      DBMS_OUTPUT.PUT_LINE(CP||' Added program to ' || RG);
    END IF;
  END IF;

  l_line  :=2;
  RG      := 'Tribus Request Group';
  RG_APPS := 'TRIBUS';
  IF ( FND_PROGRAM.REQUEST_GROUP_EXISTS ( rg, RG_APPS)) THEN
    DBMS_OUTPUT.PUT_LINE(rg||' Request Group Exists');
    IF (fnd_program.program_in_group(CP, CP_APPS, RG, RG_APPS)) THEN
      DBMS_OUTPUT.PUT_LINE(CP||' Program already exists in ' ||rg);
      NULL;
    ELSE
      fnd_program.add_to_group ( program_short_name => CP, program_application => CP_APPS, request_group => RG, group_application => RG_APPS);
      DBMS_OUTPUT.PUT_LINE(CP||' Added program to '|| RG);
    END IF;
  END IF;

  l_line  :=3;
  RG      := 'N-Tier Marketing Request Group';
  RG_APPS := 'OZF';
  IF ( FND_PROGRAM.REQUEST_GROUP_EXISTS ( rg, RG_APPS)) THEN
    DBMS_OUTPUT.PUT_LINE(rg||' Request Group Exists');
    IF (fnd_program.program_in_group(CP, CP_APPS, RG, RG_APPS)) THEN
      DBMS_OUTPUT.PUT_LINE(CP||' Program already exists in ' ||rg);
      NULL;
    ELSE
      fnd_program.add_to_group ( program_short_name => CP, program_application => CP_APPS, request_group => RG, group_application => RG_APPS);
      DBMS_OUTPUT.PUT_LINE(CP||' Added program to ' || RG);
    END IF;
  END IF;

  --------------------------

  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  dbms_output.put_line('In others Exceptions TP_OZF_ARL_SUM_RPT_CP_TO_RG.sql Line :' ||l_line|| 'RG : '|| RG   || 'CP :'|| CP ||SQLCODE||' '||SQLERRM);
END;
/
EXIT;
