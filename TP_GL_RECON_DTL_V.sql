create or replace view TRIBUS.TP_GL_RECON_DTL_V AS 
SELECT *
FROM
  (WITH offer_smry AS
  (SELECT
    /*+ MATERIALIZE */
    org_id,
    gl_ledger_id,
    sla_period_name,
    gl_period_name,
    account_number,
    party_name,
    fund_name,
    offer_name,
    Type,
    offer_code,
    offer_currency,
    ledger_currency,
    offer_status,
    ACTIVITY,
    exp_company,
    exp_account,
    LIB_COMPANY,
    LIB_ACCOUNT,
    SUM(accrd_amount) accrd_amount,
    SUM(paid_amount) paid_amount,
    SUM(BDJ_ADJ_AMOUNT) BDJ_ADJ_AMOUNT
  FROM TP_OZF_ACCRL_PAY_SUMRY
  GROUP BY org_id,
    gl_ledger_id,
    sla_period_name,
    gl_period_name,
    account_number,
    party_name,
    fund_name,
    offer_name,
    Type,
    offer_code,
    offer_currency,
    ledger_currency,
    offer_status,
    ACTIVITY,
    exp_company,
    exp_account,
    LIB_COMPANY,
    LIB_ACCOUNT
  )
SELECT org_id,
  gl_ledger_id,
  gl_period_name,
  account_number,
  party_name,
  fund_name,
  offer_name,
  Type,
  offer_code transaction,
  offer_currency,
  ledger_currency,
  offer_status,
  ACTIVITY,
  exp_company,
  exp_account,
  LIB_COMPANY,
  LIB_ACCOUNT,
  accrd_amount,
  0 paid_amount,
  BDJ_ADJ_AMOUNT
FROM offer_smry
WHERE accrd_amount > 0
OR BDJ_ADJ_AMOUNT <> 0
UNION ALL
SELECT offer_smry.org_id,
  offer_smry.gl_ledger_id,
  offer_smry.gl_period_name,
  offer_smry.account_number,
  offer_smry.party_name,
  offer_smry.fund_name,
  offer_smry.offer_name,
  offer_smry.Type,
  TOCL.claim_number transaction,
  offer_smry.offer_currency,
  offer_smry.ledger_currency,
  offer_smry.offer_status,
  offer_smry.ACTIVITY,
  NULL exp_company,
  NULL exp_account,
  TOAP.GCC_COMPANY LIB_COMPANY,
  TOAP.GCC_ACCOUNT LIB_ACCOUNT,
  0 accrd_amount,
  SUM (TOAP.ACCOUNTED_DR - TOAP.ACCOUNTED_CR) paid_amount,
  0 BDJ_ADJ_AMOUNT
FROM offer_smry,
  TP_OFFER_CLAIM_LINES TOCL,
  TP_OFFER_AE_PAYMENTS TOAP
WHERE TOAP.ACCOUNTING_CLASS_CODE = 'LIABILITY'
AND TOAP.GL_PERIOD               = offer_smry.gl_period_name
AND TOAP.gl_ledger_id            = offer_smry.gl_ledger_id
AND TOCL.CLAIM_LINE_ID           = TOAP.CLAIM_LINE_ID
AND TOCL.ACCOUNT_NUMBER          = offer_smry.ACCOUNT_NUMBER
AND offer_smry.GL_PERIOD_NAME    = toap.GL_PERIOD
AND offer_smry.sla_period_name   = toap.sla_period_name
AND offer_smry.OFFER_CODE        = toap.OFFER_CODE
AND toap.ACCOUNTING_CLASS_CODE   = 'LIABILITY'
AND offer_smry.LIB_COMPANY       = toap.GCC_COMPANY
AND offer_smry.LIB_ACCOUNT       = toap.GCC_ACCOUNT
AND offer_smry.paid_amount       > 0
GROUP BY offer_smry.org_id,
  offer_smry.gl_ledger_id,
  offer_smry.gl_period_name,
  offer_smry.account_number,
  offer_smry.party_name,
  offer_smry.fund_name,
  offer_smry.offer_name,
  offer_smry.Type,
  TOCL.claim_number,
  offer_smry.offer_currency,
  offer_smry.ledger_currency,
  offer_smry.offer_status,
  offer_smry.ACTIVITY,
  TOAP.GCC_COMPANY ,
  TOAP.GCC_ACCOUNT
  )
/
GRANT ALL ON TRIBUS.TP_GL_RECON_DTL_V TO APPS
/      
EXIT;
/
