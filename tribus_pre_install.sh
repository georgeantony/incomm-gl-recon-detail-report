SYSTEM_NAME="SYSTEM"
APPS_NAME="APPS"
TRIBUS_NAME="TRIBUS"
LOG_FILE="install.log"

# *******************************************************************
#  Function to Check the login
# *******************************************************************                                                          
CHKLOGIN(){                                                     
    if sqlplus -s /nolog <<! >/dev/null 2>&1 
	set pagesize 0 feedback off verify off heading off echo off
	WHENEVER SQLERROR EXIT 1;
        CONNECT $1 ;
        EXIT;
!
    then 
        echo OK
    else
        echo NOK
    fi
}

CONN_SID=""
while [ "$CONN_SID" = ""  ]
do
	echo "Enter Connect String SID"  | tee -a $LOG_FILE
	read CONN_SID
done         

APPS_PWD=""
while [ "$APPS_PWD" = ""  ]
do
	echo "Enter APPS password"  | tee -a $LOG_FILE
	stty -echo
	read APPS_PWD
	stty echo
done         

# *******************************************************************
#  Check if Login Id is correct else prompt to get it again
# *******************************************************************
while [  `CHKLOGIN "$APPS_NAME/$APPS_PWD@$CONN_SID" "DUAL"` = "NOK"  ]
do
	echo "Password OR Connect String SID may NOT BE CORRECT. Please Re-enter"
	
	CONN_SID=""
	while [ "$CONN_SID" = ""  ]
	do
		echo "Enter Connect String SID"  | tee -a $LOG_FILE
		read CONN_SID
	done         

	APPS_PWD=""
	while [ "$APPS_PWD" = ""  ]
	do
		echo "Enter APPS password"  | tee -a $LOG_FILE
		stty -echo
		read APPS_PWD
		stty echo
	done 
done

TRIBUS_PWD=""
while [ "$TRIBUS_PWD" = ""  ]
do
	echo "Enter TRIBUS password"  | tee -a $LOG_FILE
	stty -echo
	read TRIBUS_PWD
	stty echo
done         

# *******************************************************************
#  Check if Login Id is correct else prompt to get it again
# *******************************************************************
while [  `CHKLOGIN "$TRIBUS_NAME/$TRIBUS_PWD@$CONN_SID" "DUAL"` = "NOK"  ]
do
	echo "Password for TRIBUS may NOT BE CORRECT. Please Re-enter"
	
	TRIBUS_PWD=""
	while [ "$TRIBUS_PWD" = ""  ]
	do
		echo "Enter TRIBUS password"  | tee -a $LOG_FILE
		stty -echo
		read TRIBUS_PWD
		stty echo
	done         
done

SYSTEM_PWD=""
while [ "$SYSTEM_PWD" = ""  ]
do
    echo "Enter SYSTEM password"  | tee -a $LOG_FILE
    stty -echo
    read SYSTEM_PWD
    stty echo
done         

# *******************************************************************
#  Check if Login Id is correct else prompt to get it again
# *******************************************************************
while [  `CHKLOGIN "$SYSTEM_NAME/$SYSTEM_PWD@$CONN_SID" "DUAL"` = "NOK"  ]
do
    echo "Password for SYSTEM may NOT BE CORRECT. Please Re-enter"
    
    SYSTEM_PWD=""
    while [ "$SYSTEM_PWD" = ""  ]
    do
        echo "Enter SYSTEM password"  | tee -a $LOG_FILE
        stty -echo
        read SYSTEM_PWD
        stty echo
    done         
done

# *******************************************************************
#  Enter XML Template Upload Information
# *******************************************************************

if [ $# -eq 0 ] ; then
    printf "Enter DB Server: "
    stty echo
           read p_db_server
    stty echo
    echo " "
elif [ $# -eq 1 ] ; then
    p_apps_pass=$1;
elif [ $# -ne 1 ] ; then
    echo ""
    echo "Expecting DB Server"
    echo ""
    echo "Usage $0 <DB Server>"
    echo ""
    exit 1
fi


if [ $# -eq 0 ] ; then
    printf "Enter Port number: "
    stty echo
           read p_port_number
    stty echo
    echo " "
elif [ $# -eq 1 ] ; then
    p_apps_pass=$1;
elif [ $# -ne 1 ] ; then
    echo ""
    echo "Expecting Port number"
    echo ""
    echo "Usage $0 <Port number>"
    echo ""
    exit 1
fi

if [ $# -eq 0 ] ; then
    printf "Enter DB Name: "
    stty echo
           read p_db_name
    stty echo
    echo " "
elif [ $# -eq 1 ] ; then
    p_apps_pass=$1;
elif [ $# -ne 1 ] ; then
    echo ""
    echo "Expecting DB Name"
    echo ""
    echo "Usage $0 <DB Name>"
    echo ""
    exit 1
fi
