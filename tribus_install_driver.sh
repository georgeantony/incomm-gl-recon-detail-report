# ****************************************************************************
#  Creating View in TRIBUS schema
# ****************************************************************************

if sqlplus -s $TRIBUS_NAME/$TRIBUS_PWD @TP_GL_RECON_DTL_V.sql  >> $LOG_FILE 2>> $LOG_FILE
then
    echo "View  TP_GL_RECON_DTL_V created successfully" | tee -a $LOG_FILE
else 
    echo " Error while creating view TP_GL_RECON_DTL_V" | tee -a $LOG_FILE
    exit 1
fi

# ****************************************************************************
#  Creating synonym in APPS schema
# ****************************************************************************
if sqlplus -s $APPS_NAME/$APPS_PWD @TP_GL_RECON_DTL_V_SYN.sql  >> $LOG_FILE 2>> $LOG_FILE
then
    echo "Synonym for TP_GL_RECON_DTL_V created successfully" | tee -a $LOG_FILE
else 
    echo " Error while creating table TP_OFFER_AE_ACCRUALS" | tee -a $LOG_FILE
    exit 1
fi

# ****************************************************************************
#  Upload AOL Function
# ****************************************************************************

# ****************************************************************************
#  Upload Menu (Adding Function to Menu)
# ****************************************************************************

# ****************************************************************************
#  Value Sets
# ****************************************************************************

# ****************************************************************************
#  Upload Concurrent Programs
# ****************************************************************************

if FNDLOAD $APPS_NAME/$APPS_PWD 0 Y UPLOAD $FND_TOP/patch/115/import/afcpprog.lct TP_GL_RECON_DTL_RPT.ldt - WARNING=YES UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE >> $LOG_FILE 2>> $LOG_FILE
then
    echo "Uploaded TP_GL_RECON_DTL_RPT_CP.ldt successfully" | tee -a $LOG_FILE
else 
    echo " Error while uploading TP_GL_RECON_DTL_RPT_CP.ldt " | tee -a $LOG_FILE
    exit 1
fi

# ****************************************************************************
#  Add Concurrent Program to Request Group
# ****************************************************************************

if sqlplus -s $APPS_NAME/$APPS_PWD @TP_OZF_GL_REC_SUM_CP_TO_RG.sql  >> $LOG_FILE 2>> $LOG_FILE
then
    echo "Request Group Addition TP_OZF_GL_REC_SUM_CP_TO_RG.sql created successfully" | tee -a $LOG_FILE
else 
    echo " Error while creating equest Group Addition TP_OZF_GL_REC_SUM_CP_TO_RG.sql" | tee -a $LOG_FILE
    exit 1
fi


# ****************************************************************************
#  installing Meta data For BI publisher
# ****************************************************************************

if FNDLOAD $APPS_NAME/$APPS_PWD 0 Y UPLOAD $XDO_TOP/patch/115/import/xdotmpl.lct TP_GL_RECON_DTL_BI_COMP.ldt UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE  >> $LOG_FILE 2>> $LOG_FILE
then
    echo "Create TP_GL_RECON_DTL_BI_COMP.ldt created successfully" | tee -a $LOG_FILE
else 
    echo " Error while creating TP_GL_RECON_DTL_BI_COMP.ldt" | tee -a $LOG_FILE
    exit 1
fi

# ****************************************************************************
#  installing RTF Template 
# ****************************************************************************

if adjava oracle.apps.xdo.oa.util.XDOLoader UPLOAD \
-DB_USERNAME apps \
-DB_PASSWORD $APPS_PWD \
-JDBC_CONNECTION $p_db_server:$p_port_number:$p_db_name \
-LOB_TYPE TEMPLATE \
-LANGUAGE en \
-TERRITORY US \
-APPS_SHORT_NAME $TRIBUS_NAME \
-LOB_CODE TP_GL_RECON_DTL_RPT \
-XDO_FILE_TYPE RTF \
-FILE_NAME TP_GL_RECON_DTL_RPT.rtf \
-CUSTOM_MODE FORCE  >>  $LOG_FILE
then
    echo "Uploaded TP_GL_RECON_DTL_RPT.rtf successfully" | tee -a $LOG_FILE
else 
    echo " Error while uploading TP_GL_RECON_DTL_RPT.rtf" | tee -a $LOG_FILE
    exit 1
fi

# ****************************************************************************
#   installing XML 
# ****************************************************************************

if adjava oracle.apps.xdo.oa.util.XDOLoader UPLOAD \
-DB_USERNAME apps \
-DB_PASSWORD $APPS_PWD \
-JDBC_CONNECTION $p_db_server:$p_port_number:$p_db_name \
-LOB_TYPE DATA_TEMPLATE \
-LANGUAGE en \
-TERRITORY US \
-APPS_SHORT_NAME $TRIBUS_NAME \
-LOB_CODE TP_GL_RECON_DTL_RPT \
-XDO_FILE_TYPE XML \
-FILE_NAME TP_GL_RECON_DTL_RPT.xml \
-CUSTOM_MODE FORCE >> $LOG_FILE
then
    echo "Create TP_GL_RECON_SUM_RPT.xml created successfully" | tee -a $LOG_FILE
else 
    echo " Error while creating TP_GL_RECON_SUM_RPT.xml" | tee -a $LOG_FILE
    exit 1
fi
